//
//  CardViewHStack.swift.swift
//  Odd Finder
//
//  Created by Henry Delgado on 11/15/21.
//

import SwiftUI

struct CardViewHStack: View {
    @Binding var symbol: String
    @Binding var oddSymbol: String
    @Binding var emojisCount: Int
    @Binding var oddSymbolPosition: Int
    
    
    var body: some View {
        LazyVGrid(columns: [GridItem(.adaptive(minimum: 30, maximum: 60))], spacing: 2, content: {
            
            ForEach(0..<emojisCount, id:\.self) {num in
                CardView(symbol:  num == oddSymbolPosition
                         ? $oddSymbol : $symbol)
            }
        })
    }
}

struct CardViewHStack_Previews: PreviewProvider {
    static var previews: some View {
        CardViewHStack(symbol: Binding.constant("sad"),
                       oddSymbol: Binding.constant("shy"),
                       emojisCount: Binding.constant(160),
                       oddSymbolPosition: Binding.constant(1))
            .previewInterfaceOrientation(.landscapeRight)
    }
}
