//
//  CardView.swift
//  Odd Finder
//
//  Created by Henry Delgado on 11/15/21.
//

import SwiftUI

struct CardView: View {
    @Binding var symbol: String
    private let transition : AnyTransition = AnyTransition.move(edge: .bottom)
    var body: some View {
        VStack {
            if symbol == "smile" {
                Image(symbol)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .transition(transition)
            } else if symbol == "sad" {
                Image(symbol)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .transition(transition)
            }
            else if symbol == "neutral" {
                Image(symbol)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .transition(transition)
            }
            else if symbol == "doubt" {
                Image(symbol)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .transition(transition)
            }
            else if symbol == "cry" {
                Image(symbol)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .transition(transition)
            }
            else if symbol == "kiss" {
                Image(symbol)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .transition(transition)
            }
            else if symbol == "mute" {
                Image(symbol)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .transition(transition)
            }
            else {
                Image(symbol)
                .resizable()
                .aspectRatio(1, contentMode: .fit)
                .transition(transition)
            }
        }
        .cornerRadius(20)
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(symbol: Binding.constant("smile"))
.previewInterfaceOrientation(.landscapeRight)
    }
}

