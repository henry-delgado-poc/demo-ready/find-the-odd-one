//
//  ContentView.swift
//  Odd Finder
//
//  Created by Henry Delgado on 11/15/21.
//

import SwiftUI

struct ContentView: View {
    @State private var symbols = ["cry","doubt","kiss","smile","sad","neutral","mute", "laugh","shy"]
    
    @State private var win = false
    @State private var symbol = "smile"
    @State private var oddSymbol = "shy"
    @State private var oddSymbolPosition = 1
    @State private var emojisCount = UIDevice.current.userInterfaceIdiom == .pad ? 300 : 144
    var body: some View {
        ZStack {
            //background
            Rectangle()
                .foregroundColor(
                    Color( red:225/255, green: 255/255, blue: 250/255))
                .edgesIgnoringSafeArea(.all)
            
            Rectangle()
                .foregroundColor(
                    Color( red:209/255, green: 255/255, blue: 247/255))
                .rotationEffect(Angle(degrees: 45))
                .ignoresSafeArea(edges: .all)
            
            VStack{
                Spacer()
                //title
                HStack{
                    Image(systemName: "face.dashed.fill")
                        .foregroundColor(Color(red:249/255,green:241/255,blue:7/255))
                    
                    Text("Find the Odd One")
                        .bold()
                        .foregroundColor(.black)
                    
                    Image(systemName: "face.dashed.fill")
                        .foregroundColor(Color(red:249/255,green:241/255,blue:7/255))
                    
                }.scaleEffect(1.2)
                Spacer()
                
                //cards
                CardViewHStack(symbol: $symbol,
                               oddSymbol: $oddSymbol,
                               emojisCount: $emojisCount,
                               oddSymbolPosition: $oddSymbolPosition)
                    .padding(.horizontal, 25)
                
                Spacer()
                
                //button
                HStack(spacing:20){
                    Button (action: {
                        Next()
                    }){
                        Text("Play")
                            .bold()
                            .foregroundColor(.white)
                            .padding(.all, 10)
                            .padding([.leading, .trailing], 30)
                            .background(Color.pink)
                            .cornerRadius(20)
                    }
                    
                }
                Spacer()
            }
            
        }.animation(.easeOut, value: 0.3)
    }
    
    func Next() {
        
        let repeatImageIndex = Int.random(in: 0...self.symbols.count - 1)
        self.symbol = self.symbols[repeatImageIndex]
        
        //ensure the odd image is not the same as the repeated image
        var oddNumberIndex = Int.random(in: 0...self.symbols.count - 1)
        if oddNumberIndex == repeatImageIndex{
            repeat {
                oddNumberIndex = Int.random(in: 0...self.symbols.count - 1)
            }while (oddNumberIndex == repeatImageIndex)
        }
        
        //find the odd image
        self.oddSymbol = self.symbols[oddNumberIndex]
        
        //find the next random location for odd symbol
        self.oddSymbolPosition = Int.random(in: 0...self.emojisCount - 1)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewInterfaceOrientation(.landscapeRight)
    }
}
