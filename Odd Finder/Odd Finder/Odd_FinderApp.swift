//
//  Odd_FinderApp.swift
//  Odd Finder
//
//  Created by Henry Delgado on 11/15/21.
//

import SwiftUI

@main
struct Odd_FinderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
